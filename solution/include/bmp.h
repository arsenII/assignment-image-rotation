#ifndef BMP_H
#define BMP_H

#include "image.h"
#include "iostatus.h"
#include <stdio.h>

enum read_status from_bmp(FILE *, struct image *);
enum write_status to_bmp(FILE *, struct image const *);

#endif
