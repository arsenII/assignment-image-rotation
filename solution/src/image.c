#include "image.h"
#include <malloc.h>

DECLARE_MAYBE_SRC(image, struct image)

static inline size_t get_size(struct dimensions dims) {
  return dims.width * dims.height * sizeof(struct pixel);
}

struct maybe_image image_create(struct dimensions dims) {
  struct pixel *data = malloc(get_size(dims));
  return data == NULL ? none_image : some_image(
    (struct image) { .dims = dims, .data = data }
  );
}

void image_destroy(struct image *img) { 
  if (img != NULL) free(img->data);
}
